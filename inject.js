// this is the code which will be injected into a given page...

(function() {

	//alert('inject.js');

	if($('#toolPanel').length == 0) {
		var toolPanel = document.createElement('div');
		toolPanel.id = "toolPanel";
		toolPanel.classList.add("tool-panel");
		

		var toolHeader = document.createElement('div');
		toolHeader.classList.add("tool-header");
		var header = document.createElement('h3');
		header.textContent = "Меркурий-М";
		toolHeader.appendChild(header);

		var toolBody = document.createElement('div');
		toolBody.classList.add("tool-body");
		

		var toolFooter = document.createElement('div');
		toolFooter.classList.add("tool-footer");

		var exitButton = document.createElement('button');
		exitButton.id = "exitBtn";
		exitButton.classList.add("btn");
		exitButton.textContent = "Закрыть";
		toolFooter.appendChild(exitButton);

		toolPanel.appendChild(toolHeader);
		toolPanel.appendChild(toolBody);
		toolPanel.appendChild(toolFooter);

		document.body.appendChild(toolPanel);

		$('#exitBtn').on('click', function(e) { 
			$('#toolPanel').remove(); 
		});

	} else {
		$('#toolPanel').remove();
	}
})();

function removeEmptyElements() {
	$("form").on("submit", function (e) {
    	e.preventDefault();
    	
    	$.ajax({
    		type: $(e.target).attr('method'),
    		url: "https://mercury.vetrf.ru/gve/operatorui",
    		data: $(e.target).serialize(),
    		 success: function(data) {
    		 	counter--;
    		 }
    	});
	});

	rows = $('.innerForm .innerFormWide tbody tr');
	counter = rows.length;

	//visualization

	var div = document.createElement('div');
	div.classList.add("info-panel");

	var spanL = document.createElement('span');
	spanL.textContent = "осталось удалить: ";

	var spanR = document.createElement('span');
	spanR.classList.add("removes-left")
	spanR.textContent = "???";

	div.appendChild(spanL).appendChild(spanR);
	
	document.body.appendChild(div);
	//end of visialization


	var flag = true;
	rows.each(function(index) {
		if(flag) { flag = false; counter--;}
		else {
			data = $(this).find('td');
			if((data.eq(2).html() === undefined || data.eq(2).html() === "") &&
				(data.eq(3).html() === undefined || data.eq(3).html() === "")) {

				hrefString = data.eq(6).find('a').eq(0).attr('href');
				regExp1 = /doActionBlock\((.+)\)/g;
				match = regExp1.exec(hrefString);
				
				regExp2 = /"\S*"/g;

				formName = regExp2.exec(match[1]).toString().replace(/"/g, '');
				actionName = regExp2.exec(match[1]).toString().replace(/"/g, '');
				typeName = regExp2.exec(match[1]).toString().replace(/"/g, '');
				waybillNum = regExp2.exec(match[1]).toString().replace(/"/g, '');
				versionNum = regExp2.exec(match[1]).toString().replace(/"/g, '');
				ancorName = regExp2.exec(match[1]).toString().replace(/"/g, '');

				if(waybillNum !== undefined && waybillNum != "" &&
					versionNum !== undefined && versionNum != "") {

					form = document.forms[formName];
					form.elements["_action"].value = actionName;
					form.elements["version"].value = versionNum;
					form.elements[typeName].value = waybillNum;
					form.backUrl = "";

					$(form).submit();
				}
		 	} else {
		 		 counter--;
		 	}
		}
	});

	var interval = setInterval(function(){
		//console.log(counter);
		//spanR.textContent(counter);
		$('.removesLeft').text(counter);
	    if(counter === 0) {
	        clearInterval(interval);
	        location.reload();
	    }
	}, 700);
}
function handleRemoveBtnClick() {
  chrome.tabs.executeScript({
    file: 'scripts/removeEmptyElements.js'
  });
  window.close();
}

document.getElementById('removeBtn').addEventListener('click', handleRemoveBtnClick);
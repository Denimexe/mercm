(function() {
    $("form").on("submit", function(e) {
        e.preventDefault();

        $.ajax({
            type: $(e.target).attr('method'),
            url: "https://mercury.vetrf.ru/gve/operatorui",
            data: $(e.target).serialize(),
            success: function(data) {
                counter--;
            }
        });
    });

    rows = $('.innerForm .innerFormWide tbody tr');
    counter = rows.length;

    //visualization

    var div = document.createElement('div');
    div.classList.add("info-panel");

    var spanL = document.createElement('span');
    spanL.textContent = "осталось удалить: ";

    var spanR = document.createElement('span');
    spanR.classList.add("removes-left")
    spanR.textContent = "???";

    div.appendChild(spanL).appendChild(spanR);

    document.body.appendChild(div);
    //end of visialization


    var flag = true;
    rows.each(function(index) {
        if (flag) {
            flag = false;
            counter--;
        } else {
            data = $(this).find('td');
            if ((data.eq(2).html() === undefined || data.eq(2).html() === "") &&
                (data.eq(3).html() === undefined || data.eq(3).html() === "")) {

                hrefString = data.eq(6).find('a').eq(0).attr('href');
                regExp1 = /doActionBlock\((.+)\)/g;
                match = regExp1.exec(hrefString);

                regExp2 = /"\S*"/g;

                formName = regExp2.exec(match[1]).toString().replace(/"/g, '');
                actionName = regExp2.exec(match[1]).toString().replace(/"/g, '');
                typeName = regExp2.exec(match[1]).toString().replace(/"/g, '');
                waybillNum = regExp2.exec(match[1]).toString().replace(/"/g, '');
                versionNum = regExp2.exec(match[1]).toString().replace(/"/g, '');
                ancorName = regExp2.exec(match[1]).toString().replace(/"/g, '');

                if (waybillNum !== undefined && waybillNum != "" &&
                    versionNum !== undefined && versionNum != "") {

                    form = document.forms[formName];
                    form.elements["_action"].value = actionName;
                    form.elements["version"].value = versionNum;
                    form.elements[typeName].value = waybillNum;
                    form.backUrl = "";

                    $(form).submit();
                }
            } else {
                counter--;
            }
        }
    });

    var interval = setInterval(function() {
        //spanR.textContent(counter);
        $('.removes-left').text(counter);
        if (counter === 0) {
            $(".info-panel").remove();
            clearInterval(interval);
            location.reload();
        }
    }, 700);
})();